package cat.itb.diceroller;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

   // TextView resultTextView;
    Button rollButton, resetButton;
    int random, random2;
    ImageView dado1, dado2;
    static final int[] vector = {
        R.drawable.dice_1,R.drawable.dice_2,R.drawable.dice_3,R.drawable.dice_4,R.drawable.dice_5,R.drawable.dice_6
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        dado1 = findViewById(R.id.dado1);
        dado2 = findViewById(R.id.dado2);
        resetButton = findViewById(R.id.resetbutton);


        rollButton = findViewById(R.id.roll_button);
        rollButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Dice Rolled", Toast.LENGTH_SHORT).show();
                random = (int) (Math.random() * 6);
                random2 = (int) (Math.random() * 6);
                comprobarJackpot();
                dado1.setImageResource(vector[random]);
                dado2.setImageResource(vector[random2]);
            }

        });

        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dado1.setImageResource(R.drawable.empty_dice);
                dado2.setImageResource(R.drawable.empty_dice);
            }
        });

        dado1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                random = (int) (Math.random() * 6);
                dado1.setImageResource(vector[random]);
                comprobarJackpot();
            }
        });

        dado2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                random2 = (int) (Math.random() * 6);
                dado2.setImageResource(vector[random2]);
                comprobarJackpot();
            }
        });
    }

    public void comprobarJackpot() {
        if (random == 5 && random2 == 5){
            Toast t = Toast.makeText(MainActivity.this, "JACKPOT!", Toast.LENGTH_SHORT);
            t.setGravity(Gravity.CENTER|Gravity.TOP,0,0);
            t.show();
        }
    }

}